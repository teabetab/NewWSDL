# Travail de thèse sur la composition dynamique et automatique des services dans un système d'information orienté service de l'administration publique
## Titre de la thèse: Mise en place d'un e-government par les SOAs dans les pays en voie de développement (PED): cas du Cameroun.
Le travail de thèse consiste à proposer un modèle de composition dynamique et automatique de services dans un E-gov orientée services pour les pays en voie de développement (PED). Ce modèle sera par la suite appliqué au Cameroun. La mise sur pied d'un tel système nécessite 
de prendre en compte les contraintes liées au contexte des PED qui sont: 

* **L'accès à internet.** Le taux de pénétration d'internet dans les pays en voie de développement est très faible par rapport à celui des pays développés. De plus, le réseau n'est pas toujours de bonne qualité
(problème du débit, coupures intempestives). Le système d'informations proposé doit tenir compte de ces contraintes c'est à dire réaliser une bonne partie de ses tâches en offline. 

* **Le manque d'infrastructures.** Les PED font également face à un sérieux problème de la qualité des infrastructures disponibles dans ses administrations publiques. Les infrastructures ne sont pas toujours de bonne qualité et les ressources disponibles 
sur les machines/serveurs (mémoire, CPU, unité de stockage etc.) très souvent ne sont pas suffisantes pour héberger un service très gourmand en ressources. Notre modèle doit tenir compte de ces contraintes pour 
fournir un service de qualité nécessitant le moins de ressource possible, mais proposant une qualité de service suffisante pour satisfaire les utilisateurs. 

* **Les lenteurs administratives.** Comme on a l'habitude de le dire, l'administration publique est procédurières. Elle exige des méthodes de coopération et des liens institutionnels entre des entités horizontales suffisamment autonomes et des organisations 
verticales respectant une hiérarchie dictée par des règles. Le partage d'une information dans un e-gorvnment doit matérialiser ces procédures administratives pour les rendre automatiques et faciliter les transactions. 


## Orientation: 
Nous voulons appliquer notre système d'information au contexte camerounais et plus précisément au système judiciaire du Cameroun. 

## Contributions
Notre contribution va se situer à deux niveaux: 
    
### Contribution 1: Un nouveau modèle de description de services appelé EGov-WSDL spécifiquement adapté pour une estimation plus rapide des chemins de composition dans les systèmes d'information des administrations publiques.
Il s'agit dans cette contribution de définir un nouveau modèle de description des services dans une architecture SOA dédiée aux e-government dans les PED. Nous allons partir du modèle WSDL existant, enrichir ce modèle pour qu'il soit plus 
 adapté à la collaboration au sein de l'administration publique et l'environnement de notre contexte d'étude. Comme éléments importants pouvant enrichir notre modèle de description de services nous avons: les profils utilisateurs, les liens institutionnels (présents dans les institutions gouvernementales)
 et le pouvoir hiérarchique. Dans cette contribution nous allons extraire les informations ayant enrichi notre modèle de description des services pour optimiser les algorithmes existants pour la détermination plus rapide des chemins de composition. 
 
### Contribution 2: Découverte plus rapide des chemins de composition 
 
 Dans cette deuxième contribution, nous allons sur la base de notre nouveau modèle de descriptions des services, proposer des algorithmes de découverte des chemins de composition beaucoup plus rapides. L'idée ici est de proposer un algorithme 
 qui permet de faire un filtrage sur le graphe de composabilité descriptive des services en exploitant les contraintes administratives telles que les liens institutionnels et le pouvoir hiérarchique. 
 * Déterminer la composabilité en offline des services en utilisant les algorithmes existants. Et par la suite, filtrer les chemins de composition qui ne prennent pas en compte les contraintes administratives de l'e-government. Ces contraintes sont précisément les liens institutionnels et le pouvoir hiérarchique. Ceci permet de réduire les champs les champs de possibilité de chemins de composition et améliore par là même la sélection des services.
 * Proposer une approche incrémentale dans la découverte des chemins de compositions basée sur l'algorithme des Fourmies, c.-à-d., à chaque découverte d'un chemin notre système doit être capable de garder une trace des chemins déjà découverts pour pouvoir réutiliser ces chemins lors d'une prochaine requête similaire. Cela permettrait de réduire le temps de calcul ou encore la puissance de calcul nécessaire pour la découverte des chemins de composition. 
 * Notre algorithme doit également intégrer le fait que le chemin de composition à déterminer pour une requête utilisateur n'est pas forcément le plus cour. La fonction à majorer peut être juridique, c'est à dire qu'elle est le chemin qui nécessite le moins de transactions administratives mais subissant les mêmes contrôles. 
  
 
## Nos publications, objectifs: de contributions: 

De nos deux contributions précédentes nous devons être capables de réaliser deux publications dans des conférences scientifiques. La première conférence visée est IJICT donc la deadline est le 5 Mai 2018 


## Les défis: 
	
* **La modélisation des chemins de composition.**  On va sur l'hypothèse que la modélisation d'un chemin de composition se fera par des graphes. On considère que un service est un nœud, et un arc est possible entre deux nœuds si les services sont similaires (Input de l'un correspond à l'output de l'autre), on peut parler de la complémentarité. 

* **La modélisation des ontologies.** Une ontologie (qui sera du E-Gov dans notre cas d'espèce) correspond aux contraintes qui seront appliquées à notre graphe de chemin de composition. Donc il est question de déterminer la meilleure modélisation permettant de les appliquées aux graphs. Comme piste, il s'agit d'un article qui parle du modèle entité association (la référence doit être retrouvée xxxxxxxxxx). Donc le principale défit est de déterminer la modélisation des ontologies  qui permettra une application rapide sur les graphes.



